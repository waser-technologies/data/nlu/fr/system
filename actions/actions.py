import os
import pickle
import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
from typing import Dict, Text, List, Any, Optional

from rasa_sdk import Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from rasa_sdk.types import DomainDict

USERNAME = os.environ.get('USERNAME', 'Unknown')
HOME = "/home/%s" % USERNAME
HISTORY_FILEPATH = HOME + "/.history"


def now():
    return datetime.now()

def a_year():
    a_year = relativedelta(years=1)
    return a_year

def a_month():
    a_month = relativedelta(months=1)
    return a_month

def an_hour():
    an_hour = relativedelta(hours=1)
    return an_hour

def is_morning() -> bool:
    n = now()
    if n >= time(00,00) or n <= time(11,00):
        return True
    return False

def is_afternoon() -> bool:
    n = now()
    if n >= time(12,00) or n <= time(17,00):
        return True
    return False

def is_evening() -> bool:
    n = now()
    if n >= time(18,00) or n <= time(23,59):
        return True
    return False

def get_greetings_action_name() -> str:
    # if morning, say good morning
    if is_morning():
        return "utter_avoir_bon_je_matin"
    # if after noon, say good afternoon
    elif is_afternoon():
        return "utter_avoir_bon_je_aprèsmidi"
    # if evening, say good evening
    elif is_evening():
        return "utter_avoir_bon_je_soir"
    # else say good day
    return "utter_avoir_bon_je_jour"

def load_history():
    try:
        with open(HISTORY_FILEPATH, 'rb') as f:
            history = pickle.load(f)
            f.close()
            return history
    except Exception as e:
        return []

def get_last_timestamp_from(history):
    if history != []:
        last_converstation = history[-1]
        if last_converstation:
            last_utterance = last_converstation[-1]
            if last_utterance:
                last_timestamp = last_utterance.get('timestamp')
                return last_timestamp
    
    last_timestamp = None
    return last_timestamp

def no_current_conversation_from(history):
    number_of_minutes_afterwhich_the_last_conversation_is_no_longer_the_current_one_anymore = 3
    last_timestamp = get_last_timestamp_from(history)
    if last_timestamp != None:
        n = now()
        if ( ( n - last_timestamp ).total_seconds() / 60.0 ) < number_of_minutes_afterwhich_the_last_conversation_is_no_longer_the_current_one_anymore:
            return False
    return True

def get_current_conversation_from(history):
    if no_current_conversation_from(history):
        is_created = True
        current_conversation = []
    else:
        is_created = False
        current_conversation = history[-1]
    return current_conversation, is_created

def get_timestamp_from_previous_conv(history):
    if history != []:
        previous_converstation = history[-2]
        if previous_converstation:
            last_utterance = previous_converstation[-1]
            if last_utterance:
                timestamp_previous_conv = last_utterance.get('timestamp')
                return timestamp_previous_conv
    
    timestamp_previous_conv = None
    return timestamp_previous_conv

class ActionSaluer(Action):

    def name(self) -> Text:
        return "action_saluer"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        
        greeting_action_name = get_greetings_action_name()
        
        dispatcher.utter_message(template=greeting_action_name)

        return []

# class ActionSessionStart(Action):
#     def name(self) -> Text:
#         return "action_session_start"

#     @staticmethod
#     def fetch_slots(tracker: Tracker) -> List[EventType]:
#         """Collect slots and their values."""

#         slots = []
#         for key in tracker.keys():
#             value = tracker.get_slot(key)
#             if value is not None:
#                 slots.append(SlotSet(key=key, value=value))
#         return slots

#     async def run(
#         self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
#     ) -> List[EventType]:

#         events = [SessionStarted()]

#         events.extend(self.fetch_slots(tracker))

#         # Do we know the admin?

#         ## His first name ?
#         admin_firstname = tracker.get_slot("admin_prénom")
#         ## Last name?
#         admin_lastname = tracker.get_slot("admin_nom")
#         ## Gender of the admin?
#         admin_gender = tracker.get_slot("admin_genre")

#         # Does a previous session exists ?
#         ## If so how long has it been since we spoke to the admin ?
#         history = load_history()
#         last_seen_admin = get_timestamp_from_previous_conv(history)
#         if admin_firstname and admin_lastname and admin_gender and last_seen_admin:
#             print("\t[\tWe know the admin\t]\t")
#             n = now()
#         ### If it has been more than a year, say how long it has been
#             if ( n - last_seen_admin ) >= ( n - ( last_seen_admin + a_year() ) ):
#                 pass
#         ### If it has been more than a month, say so
#             elif (n - last_seen_admin) >= ( n - ( last_seen_admin + a_month() ) ):
#                 pass
#         ### If it has been more than an hour, say hello
#             elif (n - last_seen_admin) >= ( n - ( last_seen_admin + an_hour() ) ):
#                 greeting_action_name = get_greetings_action_name()
#                 dispatcher.utter_message(template=greeting_action_name)
#         ### If it has been less than an hour, say nothing
#             else:
#                 pass
            
#             events.append(ActionExecuted("action_listen"))
#         # No previous session, we don't know who the admin is
#         else:
#             print("\t[\tWe don\'t know the admin\t]\t")
#             greeting_action_name = get_greetings_action_name()
#             dispatcher.utter_message(template=greeting_action_name)
        
#             events.append(ActionExecuted("action_listen"))
            
#         return events


class ValidateAdminInitForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_administration_initial_form"

    @staticmethod
    def first_names_db(self) -> List[Text]:
        """Database of first names"""

        return ["Danny", "Tom", "Thomas", "Katerine", "Jean-Luc"]
    
    @staticmethod
    def last_names_db(self) -> List[Text]:
        """Database of last names"""

        return ["Waser", "Paris", "Dupont", "Janeway", "Picard"]

    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Optional[List[Text]]:
        first_name = tracker.slots.get("admin_prénom")
        if first_name is not None:
            if first_name not in self.first_names_db():
                return ["admin_prénom_écrit_correctement"] + slots_mapped_in_domain
        return slots_mapped_in_domain
    
    async def extract_admin_nom_écrit_correctement(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        intent = tracker.get_intent_of_latest_message()
        return {"admin_nom_écrit_correctement": intent == "affirmer"}

    def validate_admin_nom_écrit_correctement(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `first_name` value."""
        if tracker.get_slot("admin_nom_écrit_correctement"):
            return {"admin_prénom": tracker.get_slot("admin_prénom"), "admin_nom_écrit_correctement": True}
        return {"admin_prénom": None, "admin_nom_écrit_correctement": None}

    def validate_user_is_admin(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate user_is_admin value."""

        return {"user_is_admin": bool(slot_value)}

    def validate_admin_prénom(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate first name value."""

        # If the name is too short, it might be wrong.
        if len(slot_value) <= 1:
            dispatcher.utter_message(template="utter_être_trop_je_nom_court")
            return {"admin_prénom": None}
        else:
            return {"admin_prénom": slot_value}
    
    def validate_admin_nom(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate last name value."""

        # If the name is too short, it might be wrong.
        if len(slot_value) <= 1:
            dispatcher.utter_message(template="utter_être_trop_je_nom_court")
            return {"admin_nom": None}
        else:
            return {"admin_nom": slot_value}