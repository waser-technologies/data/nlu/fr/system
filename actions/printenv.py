import os

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional



class ActionObtenirEnvironnmentCourrant(Action): #printenv
    
    def name(self) -> Text:
        return "action_être_quel_environnment_actuel"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        
        #os.system('/usr/bin/gnome-terminal -- $SHELL -c \'printenv|$EDITOR; exec $SHELL\'')

        return []
