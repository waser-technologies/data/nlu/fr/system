import time
import random
import locale
#import inflect
#import translate

from datetime import datetime
#from polyglot import transliteration
from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

#from polyglot.transliteration import Transliterator



locale.setlocale(locale.LC_TIME,'')

#p = inflect.engine()

#translator = translate.Translator(to_lang="fr", provider="libre")
#transliterator = Transliterator(target_lang="fr")

# def try_translate(s: str) -> Text:
#     try:
#         return translator.translate(s)
#     except translate.exceptions.TranslationError as TranslationError:
#         t = transliterator.transliterate(s)
#         if t != '' and t != s:
#             return t
#     return s

# def spell_num(n: int) -> Text:
#     # Ok this is a real cheat, but it's too easy not to use
#     # Also it a bit slow.
#     w = p.number_to_words(n) # spell out the number in english using inflect
#     try:
#         return translator.translate(w)
#     except translate.exceptions.TranslationError as TranslationError:
#         t = transliterator.transliterate(w)
#         if t != '' and t != w:
#             return t
#     return str(n) # last resort if everything else fails


class ActionDonnerDate(Action):

    def name(self) -> Text:
        return "action_être_quel_il_date"

    def spell_number(self, n: int) -> Text:
        n_dict = {
            '0': "",
            '1': "un",
            '2': "deux",
            '3': "trois",
            '4': "quatre",
            '5': "cinq",
            '6': "six",
            '7': "sept",
            '8': "huit",
            '9': "neuf",
            '10': "dix",
            '11': "onze",
            '12': "douze",
            '13': "treize",
            '14': "quatorze",
            '15': "quinze",
            '16': "seize",
            '20': "vingt",
            '30': "trente",
            '40': "quarante",
            '50': "cinquante",
            '60': "soixante",
            '70': "septante",
            '80': "huitante",
            '90': "nonante",
            '100': "cent",
            '1000': "mille"
        }

        if n > 16 and n < 100:
            _d = int( n / 10 ) * 10
            d = n_dict.get(str(_d))
            _u = int(n - _d)
            u = n_dict.get(str(_u))
            return f"{d}-{u}"
        elif n > 100 and n < 1000:
            # 999
            _c = int( n / 100 ) #9
            _w = 100 #100
            _d = int( (n - _c*_w) / 10 ) * 10 #90
            _u = int( n - _c*_w - _d ) #9

            c = n_dict.get(str(_c))
            w = n_dict.get(str(_w))
            d = n_dict.get(str(_d))
            u = n_dict.get(str(_u))

            return f"{c}-{w}-{d}-{u}"
        elif n > 1000 and n < 1000000:
            # 2021
            _m = int( n / 1000) #2
            _m_c = int( _m / 100 ) #0
            _m_c_w = 100 #100
            _m_d = int( (_m - _m_c*_m_c_w) / 10 ) * 10 #0
            _m_u = int( _m - _m_c*_m_c_w - _m_d ) #2
            _m_w = 1000 #1000
            _c_ = int( n - _m*_m_w ) #21
            _c = int( _c_ / 100 ) #0
            _c_w = 100 #100
            _d_ = int( _c_ - _c*_c_w ) #21
            _d = int( _d_ / 10 ) * 10 #20
            _u = int( _d_ - _d ) #1


            m = n_dict.get(str(_m_c)) #''
            m_d_w = n_dict.get(str(_m_c_w)) #cent
            m_d = n_dict.get(str(_m_d)) #''
            m_u = n_dict.get(str(_m_u)) #deux
            m_w = n_dict.get(str(_m_w)) #mille
            c = n_dict.get(str(_c)) #''
            c_w = n_dict.get(str(_c_w)) #cent
            d = n_dict.get(str(_d)) #vingt
            u = n_dict.get(str(_u)) #un

            if _u == 1:
                u = "et-un"

            if m:
                return f"{m}-{m_d_w}-{m_d}-{m_u} {m_w} {c}-{c_w}-{d}-{u}" # TODO: add if statements for all units.
            elif m_d:
                return f"{m_d}-{m_u} {m_w} {c}-{c_w}-{d}-{u}" # TODO: add if statements for all units.
            elif m_u:
                if _m_u == 1:
                    if c:
                        if d:
                            if u:
                                return f"{m_w} {c}-{c_w}-{d}-{u}"
                            else:
                                return f"{m_w} {c}-{c_w}-{d}"
                        else:
                            if u:
                                return f"{m_w} {c}-{c_w}-{u}"
                            else:
                                return f"{m_w} {c}-{c_w}"
                    else:
                        if d:
                            if u:
                                return f"{m_w}-{d}-{u}"
                            else:
                                return f"{m_w}-{d}"
                        else:
                            if u:
                                return f"{m_w}-{u}"
                            else:
                                return f"{m_w}"
                else:
                    if c:
                        if d:
                            if u:
                                return f"{m_u} {m_w} {c}-{c_w}-{d}-{u}"
                            else:
                                return f"{m_u} {m_w} {c}-{c_w}-{d}"
                        else:
                            if u:
                                return f"{m_u} {m_w} {c}-{c_w}-{u}"
                            else:
                                return f"{m_u} {m_w} {c}-{c_w}"
                    else:
                        if d:
                            if u:
                                return f"{m_u} {m_w} {d}-{u}"
                            else:
                                return f"{m_u} {m_w} {d}"
                        else:
                            if u:
                                return f"{m_u} {m_w} {u}"
                            else:
                                return f"{m_u} {m_w}"
        else:
            return n_dict.get(str(n))

    def get_ordinal(self, n: int) -> Text:
        ordict = {
            '1': "premier",
        }
        sn = self.spell_number(n)
        if sn[-1] == "f":
            sn = str(sn + ":").replace("f:", "v") + "ième"
        return ordict.get(str(n), sn)

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        day_name = time.strftime('%A')
        day_num = self.spell_number(int(time.strftime('%d')))

        #month_num = time.strftime('%m')
        month_name = time.strftime('%B')
        
        year_num = int(time.strftime('%Y'))
        year_name = self.spell_number(year_num)

        day_num_ordinal = self.get_ordinal(int(time.strftime('%d')))
        month_name_ordinal = f"d'{month_name}" if month_name[0].lower() in ['a', 'e', 'o', 'u', 'y'] else f"de {month_name}"

        dispatcher.utter_message(response="utter_être_quel_il_date", jour_semaine=day_name, jour=day_num, mois=month_name, année=year_name, jour_ordinal=day_num_ordinal, mois_ordinal=month_name_ordinal)

        return []

class ActionDonnerHeure(Action):

    def name(self) -> Text:
        return "action_être_quel_il_heure"

    def spell_number(self, n: int) -> Text:
        """2 -> deux"""
        n_dict = {
            '0': "",
            '1': "une",
            '2': "deux",
            '3': "trois",
            '4': "quatre",
            '5': "cinq",
            '6': "six",
            '7': "sept",
            '8': "huit",
            '9': "neuf",
            '10': "dix",
            '11': "onze",
            '12': "douze",
            '13': "treize",
            '14': "quatorze",
            '15': "quinze",
            '16': "seize",
            '20': "vingt",
            '30': "trente",
            '40': "quarante",
            '50': "cinquante",
            '60': "soixante"
        }
        if n > 16:
            _d = int( n / 10 ) * 10
            d = n_dict.get(str(_d))
            _u = int(n - _d)
            u = n_dict.get(str(_u))

            if _u == 1:
                u = "et-une"

            return f"{d}-{u}"
        else:
            return n_dict.get(str(n))

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        
        half_hour_flag = False

        date = datetime.now()
        _h = int(date.hour)
        _m = int(date.minute)

        if _m < 40:
            # Heure spéciales
            # h = 0
            if _h == 0:
                if _m == 0:
                    # Il est minuit.
                    dispatcher.utter_message(text="Il est minuit.")
                    return []
                h = "minuit"
            # h = 12
            elif _h == 12:
                if _m == 0:
                    dispatcher.utter_message(text="Il est midi.")
                    return []
                h = "midi"
            # h > 12
            elif _h > 12:
                bais = [0, 0, 12, 0, 12]
                b = random.choice(bais)
                if b == 12:
                    half_hour_flag = True
                h = self.spell_number(_h - b)
            else:
                h = self.spell_number(_h)

            # minutes spéciales
            # 00
            if _m == 0:
                # il est _h heure pile.
                dispatcher.utter_message(response="utter_être_quel_il_heures", heures=h)
                return []
            # 15
            if _m == 15:
                # il est _h-12 heures et quart.
                if _h > 12 and half_hour_flag == True: # you don't say 18 heures et quart... but 6 heures et quart
                    m = "et quart"
                    dispatcher.utter_message(response="utter_être_quel_il_heures_minutes", heures=h, minutes=m)
                    return []
                else:
                    m = self.spell_number(_m)
                    dispatcher.utter_message(response="utter_être_quel_il_heures_minutes", heures=h, minutes=m)
                    return []
            # 30
            elif _m == 30:
                # il est _h-12 heures et de-mi.
                if _h > 12 and half_hour_flag == True: # you don't say 18 heures et de-mi... but 6 heures et de-mi
                    m = "et de-mi"
                    dispatcher.utter_message(response="utter_être_quel_il_heures_minutes", heures=h, minutes=m)
                    return []
                else:
                    m = self.spell_number(_m)
                    dispatcher.utter_message(response="utter_être_quel_il_heures_minutes", heures=h, minutes=m)
                    return []
            else:
                m = self.spell_number(_m)
                dispatcher.utter_message(response="utter_être_quel_il_heures_minutes", heures=h, minutes=m)
                return []
        else:
            # Il est 10 heures -20
            if _h < 12:
                _h += 1
                _m = 60 - _m
            elif _h > 12:
                b = 12
                _h = _h - b + 1
                _m = 60 - _m
            
            if _h == 0:
                h = "minuit"
                m = self.spell_number(_m)

                dispatcher.utter_message(response="utter_être_quel_il_temps", heures=h, minutes=m)
                return []
            elif _h == 12:
                h = "midi"
                m = self.spell_number(_m)

                dispatcher.utter_message(response="utter_être_quel_il_temps", heures=h, minutes=m)
                return []
            else:
                h = self.spell_number(_h)

                if _m == 15:
                    m = "quart"
                else:
                    m = self.spell_number(_m)
                
                dispatcher.utter_message(response="utter_être_quel_il_heures_minutes_moins", heures=h, minutes=m)
                return []
        return []