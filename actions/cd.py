import os
import pickle
import pathlib

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

USERNAME = os.environ.get("USERNAME", '')
HOME = pathlib.Path(__file__).parent.parent.parent.parent.parent.resolve().as_posix() #Dirty I know..
ASSISTANT_PATH = HOME + "/.assistant"

def get_env():
    try:
        with open(f'{ASSISTANT_PATH}/.env', 'rb') as f:
            env = pickle.load(f)
            f.close()
            return env
    except FileNotFoundError as f:
        return {}

def save_env(env):
	with open(f'{ASSISTANT_PATH}/.env', 'wb') as f:
		pickle.dump(env, f)
		f.close()
	return env

def set_env(key: str, val: str):
	env = get_env()
	env[key] = val
	env = save_env(env)
	return env

def cwd(dir: str):
    dir_path = pathlib.Path(dir).resolve().as_posix()
    set_env('PWD', dir_path)
    return dir_path

class ActionChangerDossierCourrant(Action): #cd
    
    def name(self) -> Text:
        return "action_definir_dossier_il_actuel"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        dir = tracker.get_slot('cwd', HOME)
        pwd = cwd(dir)
        dispatcher.utter_message(response="utter_être_quel_dossier_actuel", pwd=pwd)

        return []
