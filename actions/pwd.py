import os
import pickle
import pathlib

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, SessionStarted, ActionExecuted, EventType
from typing import Dict, Text, List, Any, Optional

USERNAME = os.environ.get("USERNAME", '')
HOME = pathlib.Path(__file__).parent.parent.parent.parent.parent.resolve().as_posix() #Dirty I know..
ASSISTANT_PATH = HOME + "/.assistant"

def get_env():
    try:
        with open(f'{ASSISTANT_PATH}/.env', 'rb') as f:
            env = pickle.load(f)
            f.close()
            return env
    except FileNotFoundError as f:
        return {}

def get_pwd():
	env = get_env()
	return env.get('PWD', os.environ.get('PWD', HOME))

class ActionObtenirDossierCourrant(Action): #PWD
    
    def name(self) -> Text:
        return "action_être_quel_dossier_actuel"
    
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        pwd = get_pwd()
        dispatcher.utter_message(response="utter_être_quel_dossier_actuel", pwd=pwd)

        return []
