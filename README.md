# Système

Système est un domaine de compatibilité avec votre système.
Il contient non seulement les intentions nécessaires à la gestion du matériel mais aussi les actions correspondantes.

## Installation

Utilisez l'[Outil de Gestion de Domaines](https://gitlab.com/waser-technologies/technologies/dmt) (DMT en anglais) pour installer, évaluer et entrainer un modèle sur ce domaine.

```zsh
dmt -V -T -a https://gitlab.com/waser-technologies/data/nlu/fr/system.git
```

## Utilisation

Utiliser la commande `dmt` pour lancer un serveur avec le dernier modèle entraîné.

```zsh
dmt -S
```

Cela va prendre du temps.

Attendez de voir:
> `INFO     root  - Rasa server is up and running.`

Vous pourrez alors tester le modèle.

```zsh
curl -XPOST localhost:5005/webhooks/rest/webhook -s -d '{ "message": "Salut", "sender": "utilisateur" }'
```

Ou utilisez simplement [`Assistant`](https://gitlab.com/waser-technologies/technologies/assistant).

```zsh
assistant Salut
```
